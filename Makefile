OBJECTS = main.o
OUTPUT = toolchaingen

all: $(OUTPUT)
%.o: %.c
	gcc -Wall -pedantic -c -o $@ $<
$(OUTPUT): $(OBJECTS)
	gcc  -o $(OUTPUT) $(OBJECTS)
clean:
	rm -f $(OUTPUT) $(OBJECTS)
