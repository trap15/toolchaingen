/*
	Toolchaingen -- A quick and easy toolchain generator

Copyright (C) 2007 Segher Boessenkool <segher@kernel.crashing.org>
Copyright (C) 2009 Hector Martin "marcan" <hector@marcansoft.com>
Copyright (C) 2009 Andre Heider "dhewg" <dhewg@wiibrew.org>
Copyright (C) 2010-2011 Alex Marshall "trap15" <trap15@raidenii.net>

# Released under the terms of the GNU GPL, version 2
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
	char *ver;
	char *url;
	char *dir;
	char *tarball;
	char *buildopts;
	int to_build;
} Tooltarget;

typedef struct {
	Tooltarget binutils;
	Tooltarget gmp;
	Tooltarget mpfr;
	Tooltarget mpc;
	Tooltarget gcc;
	Tooltarget gdb;
	Tooltarget newlib;
	char *name;
	char *target;
	char *envvar;
} Toolchain;

void free_tooltarget(Tooltarget* tt)
{
	if(tt->ver)
		free(tt->ver);
	if(tt->url)
		free(tt->url);
	if(tt->dir)
		free(tt->dir);
	if(tt->tarball)
		free(tt->tarball);
	if(tt->buildopts)
		free(tt->buildopts);
	tt->to_build = 0;
}

void free_toolchain(Toolchain* cfg)
{
	if(cfg->name)
		free(cfg->name);
	if(cfg->target)
		free(cfg->target);
	if(cfg->envvar)
		free(cfg->envvar);
	free_tooltarget(&cfg->binutils);
	free_tooltarget(&cfg->gmp);
	free_tooltarget(&cfg->mpfr);
	free_tooltarget(&cfg->mpc);
	free_tooltarget(&cfg->gcc);
	free_tooltarget(&cfg->gdb);
	free_tooltarget(&cfg->newlib);
}

void print_tooltarget(Tooltarget* tt, char *name)
{
	printf("[%s]\n", name);
	printf("@build %s\n", tt->to_build ? "yes" : "no");
	printf("@ver %s\n", tt->ver);
	printf("@dir %s\n", tt->dir);
	printf("@tarball %s\n", tt->tarball);
	printf("@url %s\n", tt->url);
	printf("@opts %s\n\n", tt->buildopts);
}

void print_toolchain(Toolchain* cfg)
{
	printf("[toolchain]\n");
	printf("@name %s\n", cfg->name);
	printf("@target %s\n", cfg->target);
	printf("@envvar %s\n\n", cfg->envvar);
	print_tooltarget(&cfg->binutils, "binutils");
	print_tooltarget(&cfg->gmp, "gmp");
	print_tooltarget(&cfg->mpfr, "mpfr");
	print_tooltarget(&cfg->mpc, "mpc");
	print_tooltarget(&cfg->gcc, "gcc");
	print_tooltarget(&cfg->gdb, "gdb");
	print_tooltarget(&cfg->newlib, "newlib");
}

void load_toolchain_script(Toolchain* cfg, char *file)
{
	char tmpbuf[256];
	int tgt = -1;
	FILE* fp;
	int keep_going = 1;
	fp = fopen(file, "rb");
	if(fp == NULL) {
		fprintf(stderr, "Can't open for reading\n");
		exit(EXIT_FAILURE);
	}
	while(keep_going) {
		if(fgets(tmpbuf, 255, fp) == NULL)
			break;
		if(feof(fp))
			keep_going = 0;
		if(tmpbuf[0] == '[') {
			if(strcmp(tmpbuf, "[toolchain]\n") == 0) {
				tgt = 0;
			}else if(strcmp(tmpbuf, "[gcc]\n") == 0) {
				tgt = 1;
			}else if(strcmp(tmpbuf, "[binutils]\n") == 0) {
				tgt = 2;
			}else if(strcmp(tmpbuf, "[gmp]\n") == 0) {
				tgt = 3;
			}else if(strcmp(tmpbuf, "[mpfr]\n") == 0) {
				tgt = 4;
			}else if(strcmp(tmpbuf, "[mpc]\n") == 0) {
				tgt = 5;
			}else if(strcmp(tmpbuf, "[gdb]\n") == 0) {
				tgt = 6;
			}else if(strcmp(tmpbuf, "[newlib]\n") == 0) {
				tgt = 7;
			}else
				tgt = -1;
		}else if(tmpbuf[0] == '@') {
			if(tgt == 0) {
				if(strncmp(tmpbuf + 1, "name ", 4) == 0) {
					if(cfg->name)
						free(cfg->name);
					cfg->name = malloc(strlen(tmpbuf) - 6);
					snprintf(cfg->name, strlen(tmpbuf) - 6, "%s", tmpbuf + 6);
				}else if(strncmp(tmpbuf + 1, "target ", 6) == 0) {
					if(cfg->target)
						free(cfg->target);
					cfg->target = malloc(strlen(tmpbuf) - 8);
					snprintf(cfg->target, strlen(tmpbuf) - 8, "%s", tmpbuf + 8);
				}else if(strncmp(tmpbuf + 1, "envvar ", 6) == 0) {
					if(cfg->envvar)
						free(cfg->envvar);
					cfg->envvar = malloc(strlen(tmpbuf) - 8);
					snprintf(cfg->envvar, strlen(tmpbuf) - 8, "%s", tmpbuf + 8);
				}
			}else{
				Tooltarget* tt;
				switch(tgt) {
					case 1: tt = &cfg->gcc; break;
					case 2: tt = &cfg->binutils; break;
					case 3: tt = &cfg->gmp; break;
					case 4: tt = &cfg->mpfr; break;
					case 5: tt = &cfg->mpc; break;
					case 6: tt = &cfg->gdb; break;
					case 7: tt = &cfg->newlib; break;
					default: tt = NULL; break;
				}
				if(tt == NULL)
					continue;
				if(strncmp(tmpbuf + 1, "ver ", 3) == 0) {
					if(tt->ver)
						free(tt->ver);
					tt->ver = malloc(strlen(tmpbuf) - 5);
					snprintf(tt->ver, strlen(tmpbuf) - 5, "%s", tmpbuf + 5);
				}else if(strncmp(tmpbuf + 1, "dir ", 3) == 0) {
					if(tt->dir)
						free(tt->dir);
					tt->dir = malloc(strlen(tmpbuf) - 5);
					snprintf(tt->dir, strlen(tmpbuf) - 5, "%s", tmpbuf + 5);
				}else if(strncmp(tmpbuf + 1, "tarball ", 7) == 0) {
					if(tt->tarball)
						free(tt->tarball);
					tt->tarball = malloc(strlen(tmpbuf) - 9);
					snprintf(tt->tarball, strlen(tmpbuf) - 9, "%s", tmpbuf + 9);
				}else if(strncmp(tmpbuf + 1, "url ", 3) == 0) {
					if(tt->url)
						free(tt->url);
					tt->url = malloc(strlen(tmpbuf) - 5);
					snprintf(tt->url, strlen(tmpbuf) - 5, "%s", tmpbuf + 5);
				}else if(strncmp(tmpbuf + 1, "build ", 5) == 0) {
					if(strncmp(tmpbuf + 7, "yes\n", 3) == 0)
						tt->to_build = 1;
					else
						tt->to_build = 0;
				}else if(strncmp(tmpbuf + 1, "opts ", 4) == 0) {
					if(tt->buildopts)
						free(tt->buildopts);
					tt->buildopts = malloc(strlen(tmpbuf) - 6);
					snprintf(tt->buildopts, strlen(tmpbuf) - 6, "%s", tmpbuf + 6);
				}
			}
		}
	}
}

void setup_tooltarget(Tooltarget* tgt, char *ver, char *dir, char *tb, char *url, char *bops, int build)
{
	asprintf(&tgt->ver, ver);
	asprintf(&tgt->dir, dir);
	asprintf(&tgt->tarball, tb);
	asprintf(&tgt->url, url);
	asprintf(&tgt->buildopts, bops);
	tgt->to_build = build;
}

void load_toolchain_defaults(Toolchain* cfg)
{
	asprintf(&cfg->name, "PowerPC");
	asprintf(&cfg->target, "powerpc-elf");
	asprintf(&cfg->envvar, "PPCDEV");
	
	setup_tooltarget(&cfg->binutils,
			 "2.20.1",
			 "binutils-$BINUTILS_VER",
			 "binutils-$BINUTILS_VER.tar.bz2",
			 "http://ftp.gnu.org/gnu/binutils/$BINUTILS_TARBALL",
			 "",
			 1);
	
	setup_tooltarget(&cfg->gmp,
			 "5.0.1",
			 "gmp-$GMP_VER",
			 "gmp-$GMP_VER.tar.bz2",
			 "http://ftp.gnu.org/gnu/gmp/$GMP_TARBALL",
			 "",
			 1);
	
	setup_tooltarget(&cfg->mpfr,
			 "2.4.2",
			 "mpfr-$MPFR_VER",
			 "mpfr-$MPFR_VER.tar.bz2",
			 "http://www.mpfr.org/mpfr-$MPFR_VER/$MPFR_TARBALL",
			 "",
			 1);
	
	setup_tooltarget(&cfg->mpc,
			 "0.8.2",
			 "mpc-$MPC_VER",
			 "mpc-$MPC_VER.tar.gz",
			 "http://www.multiprecision.org/mpc/download/$MPC_TARBALL",
			 "",
			 1);
	
	setup_tooltarget(&cfg->gcc,
			 "4.4.4",
			 "gcc-$GCC_VER",
			 "gcc-$GCC_VER.tar.bz2",
			 "http://ftp.gnu.org/gnu/gcc/gcc-$GCC_VER/$GCC_TARBALL",
			 "",
			 1);
	
	setup_tooltarget(&cfg->gdb,
			 "7.1",
			 "gdb-$GDB_VER",
			 "gdb-$GDB_VER.tar.bz2",
			 "http://ftp.gnu.org/gnu/gdb/$GDB_TARBALL",
			 "",
			 1);
	
	setup_tooltarget(&cfg->newlib,
			 "1.18.0",
			 "newlib-$NEWLIB_VER",
			 "newlib-$NEWLIB_VER.tar.gz",
			 "ftp://sources.redhat.com/pub/newlib/$NEWLIB_TARBALL",
			 "",
			 1);
}

#define TOOLSCRIPT_HEADER	\
"#!/bin/sh\n" \
"\n" \
"# Copyright (C) 2007 Segher Boessenkool <segher@kernel.crashing.org>\n" \
"# Copyright (C) 2009 Hector Martin \"marcan\" <hector@marcansoft.com>\n" \
"# Copyright (C) 2009 Andre Heider \"dhewg\" <dhewg@wiibrew.org>\n" \
"# Copyright (C) 2010-2011 Alex Marshall \"trap15\" <trap15@raidenii.net>\n" \
"\n" \
"# Released under the terms of the GNU GPL, version 2\n" \
"SCRIPTDIR=`dirname $PWD/$0`\n" \
"\n"

#define TOOLSCRIPT_TOP_SETTING \
"if [ -z $MAKEOPTS ]; then\n" \
"	MAKEOPTS=-j3\n" \
"fi\n" \
"\n" \
"# End of configuration section.\n" \
"\n" \
"case `uname -s` in\n" \
"	*BSD*)\n" \
"		MAKE=gmake\n" \
"		;;\n" \
"	*)\n" \
"		MAKE=make\n" \
"esac\n" \
"\n" \
"export PATH=$TOOLDEV/bin:$PATH\n" \
"\n" \
"die() {\n" \
"	echo $@\n" \
"	exit 1\n" \
"}\n" \
"\n"

#define TOOLSCRIPT_DOWNLOAD_SUBS \
"download() {\n" \
"	DL=1\n" \
"	if [ -f \"$TOOLDEV/$2\" ]; then\n" \
"		echo \"Testing $2...\"\n" \
"# Check bz2 and gz\n" \
"		tar tjf \"$TOOLDEV/$2\" >/dev/null 2>&1 && DL=0\n" \
"		if [ $DL -eq 1 ]; then\n" \
"			tar tzf \"$TOOLDEV/$2\" >/dev/null 2>&1 && DL=0\n" \
"		fi\n" \
"	fi\n" \
"\n" \
"	if [ $DL -eq 1 ]; then\n" \
"		echo \"Downloading $2...\"\n" \
"		wget \"$1\" -c -O \"$TOOLDEV/$2\" || die \"Could not download $2\"\n" \
"	fi\n" \
"}\n" \
"\n" \
"extract() {\n" \
"	echo \"Extracting $1...\"\n" \
"	tar xf \"$TOOLDEV/$1\" -C \"$2\" || die \"Error unpacking $1\"\n" \
"}\n" \
"\n"

#define TOOLSCRIPT_FOOTER \
"if [ -z \"$TOOLDEV\" ]; then\n" \
"	die \"Please set $TOOLDEV in your environment.\"\n" \
"fi\n" \
"\n" \
"if [ $# -eq 0 ]; then\n" \
"	die \"Please specify build type(s) (toolchain/clean)\"\n" \
"fi\n" \
"\n" \
"if [ \"$1\" = \"all\" ]; then\n" \
"	buildtoolchain\n" \
"	cleanbuild\n" \
"	cleansrc\n" \
"	exit 0\n" \
"fi\n" \
"\n" \
"while true; do\n" \
"	if [ $# -eq 0 ]; then\n" \
"		exit 0\n" \
"	fi\n" \
"	case $1 in\n" \
"		toolchain)	buildtoolchain ;;\n" \
"		clean)		cleanbuild; cleansrc; exit 0 ;;\n" \
"		*)\n" \
"			die \"Unknown build type $1\"\n" \
"			;;\n" \
"	esac\n" \
"	shift;\n" \
"done\n" \
"\n" \

void write_tooltarget(FILE* fp, Tooltarget tt, char *pfx)
{
	if(tt.to_build) {
		fprintf(fp, "# %s config\n", pfx);
		fprintf(fp, "%s_VER=%s\n", pfx, tt.ver);
		fprintf(fp, "%s_DIR=\"%s\"\n", pfx, tt.dir);
		fprintf(fp, "%s_TARBALL=\"%s\"\n", pfx, tt.tarball);
		fprintf(fp, "%s_URI=\"%s\"\n\n", pfx, tt.url);
	}
}

void write_configuration(FILE* fp, Toolchain cfg)
{
	write_tooltarget(fp, cfg.binutils, "BINUTILS");
	write_tooltarget(fp, cfg.gmp, "GMP");
	write_tooltarget(fp, cfg.mpfr, "MPFR");
	write_tooltarget(fp, cfg.mpc, "MPC");
	write_tooltarget(fp, cfg.gcc, "GCC");
	write_tooltarget(fp, cfg.gdb, "GDB");
	write_tooltarget(fp, cfg.newlib, "NEWLIB");
	fprintf(fp, "TOOLCHAIN_TARGET=%s\n\n", cfg.target);
	fprintf(fp, "TOOLDEV=$%s\n\n", cfg.envvar);
}

void write_clean_subs(FILE* fp, Toolchain cfg)
{
	/* Clean src */
	fprintf(fp, "cleansrc() {\n");
	if(cfg.binutils.to_build)
		fprintf(fp, "	[ -e $TOOLDEV/$BINUTILS_DIR ] && rm -rf $TOOLDEV/$BINUTILS_DIR\n");
	if(cfg.gcc.to_build)
		fprintf(fp, "	[ -e $TOOLDEV/$GCC_DIR ] && rm -rf $TOOLDEV/$GCC_DIR\n");
	if(cfg.gdb.to_build)
		fprintf(fp, "	[ -e $TOOLDEV/$GDB_DIR ] && rm -rf $TOOLDEV/$GDB_DIR\n");
	fprintf(fp, "}\n");
	
	/* Clean build */
	fprintf(fp, "cleanbuild() {\n");
	if(cfg.binutils.to_build)
		fprintf(fp, "	[ -e $TOOLDEV/build_binutils ] && rm -rf $TOOLDEV/build_binutils\n");
	if(cfg.gcc.to_build)
		fprintf(fp, "	[ -e $TOOLDEV/build_gcc ] && rm -rf $TOOLDEV/build_gcc\n");
	if(cfg.gdb.to_build)
		fprintf(fp, "	[ -e $TOOLDEV/build_gdb ] && rm -rf $TOOLDEV/build_gdb\n");
	if(cfg.newlib.to_build)
		fprintf(fp, "	[ -e $TOOLDEV/build_newlib ] && rm -rf $TOOLDEV/build_newlib\n");
	fprintf(fp, "}\n");
}

void write_makedirs_sub(FILE* fp, Toolchain cfg)
{
	fprintf(fp, "makedirs() {\n");
	if(cfg.binutils.to_build)
		fprintf(fp, "	mkdir -p $TOOLDEV/build_binutils || die \"Error making Binutils build directory $TOOLDEV/build_binutils\"\n");
	if(cfg.gcc.to_build)
		fprintf(fp, "	mkdir -p $TOOLDEV/build_gcc || die \"Error making GCC build directory $TOOLDEV/build_gcc\"\n");
	if(cfg.gdb.to_build)
		fprintf(fp, "	mkdir -p $TOOLDEV/build_gdb || die \"Error making GDB build directory $TOOLDEV/build_gdb\"\n");
	if(cfg.newlib.to_build)
		fprintf(fp, "	mkdir -p $TOOLDEV/build_newlib || die \"Error making Newlib build directory $TOOLDEV/build_newlib\"\n");
	fprintf(fp, "}\n");
}

void write_buildbinutils_sub(FILE* fp, Toolchain cfg)
{
	if(!cfg.binutils.to_build)
		return;
	fprintf(fp,
		"buildbinutils() {\n"
		"	TARGET=$1\n"
		"	(\n"
		"		cd $TOOLDEV/build_binutils && \\\n"
		"		$TOOLDEV/$BINUTILS_DIR/configure --target=$TARGET --prefix=$TOOLDEV \\\n");
	fprintf(fp, "%s \\\n", cfg.binutils.buildopts);
	fprintf(fp,
		"			--disable-werror --disable-multilib && \\\n"
		"		$MAKE $MAKEOPTS && \\\n"
		"		$MAKE install\n"
		"	) || die \"Error building Binutils for target $TARGET\"\n"
		"}\n");
}

void write_buildgcc_sub(FILE* fp, Toolchain cfg)
{
	if(!cfg.gcc.to_build)
		return;
	fprintf(fp,
		"buildgcc() {\n"
		"	TARGET=$1\n");
	if(cfg.newlib.to_build)
		fprintf(fp, "	NEWLIBFLAG=$2\n");
	fprintf(fp,
		"	(\n"
		"		cd $TOOLDEV/build_gcc && \\\n"
		"		$TOOLDEV/$GCC_DIR/configure --target=$TARGET --prefix=$TOOLDEV \\\n");
	fprintf(fp, "%s \\\n", cfg.gcc.buildopts);
	fprintf(fp,
		"			--disable-werror --disable-multilib \\\n");
	if(cfg.newlib.to_build)
		fprintf(fp, "			$NEWLIBFLAG\n");
	fprintf(fp,
		"	&& \\\n"
		"		$MAKE $MAKEOPTS && \\\n"
		"		$MAKE install\n"
		"	) || die \"Error building GCC for target $TARGET\"\n"
		"}\n");
}

void write_buildnewlib_sub(FILE* fp, Toolchain cfg)
{
	if(!cfg.newlib.to_build)
		return;
	fprintf(fp,
		"buildnewlib() {\n"
		"	TARGET=$1\n"
		"	(\n"
		"		cd $TOOLDEV/build_newlib && \\\n"
		"		$TOOLDEV/$NEWLIB_DIR/configure --target=$TARGET --prefix=$TOOLDEV \\\n");
	fprintf(fp, "%s \\\n", cfg.newlib.buildopts);
	fprintf(fp,
		"			&& \\\n"
		"		$MAKE $MAKEOPTS && \\\n"
		"		$MAKE install\n"
		"	) || die \"Error building Newlib for target $TARGET\"\n"
		"}\n");
}

void write_buildgdb_sub(FILE* fp, Toolchain cfg)
{
	if(!cfg.gdb.to_build)
		return;
	fprintf(fp,
		"buildgdb() {\n"
		"	TARGET=$1\n"
		"	(\n"
		"		cd $TOOLDEV/build_gdb && \\\n"
		"		$TOOLDEV/$GDB_DIR/configure --target=$TARGET --prefix=$TOOLDEV \\\n");
	fprintf(fp, "%s \\\n", cfg.gdb.buildopts);
	fprintf(fp,
		"			--disable-werror --disable-multilib --disable-sim && \\\n"
		"		$MAKE $MAKEOPTS && \\\n"
		"		$MAKE install\n"
		"	) || die \"Error building GDB for target $TARGET\"\n"
		"}\n");
}

void write_buildtarget_sub(FILE* fp, Toolchain cfg)
{
	fprintf(fp, 
		"buildtoolchain() {\n"
		"	echo \"******* Building %s toolchain\"\n", cfg.name);
	if(cfg.binutils.to_build)
		fprintf(fp, "	download \"$BINUTILS_URI\" \"$BINUTILS_TARBALL\"\n");
	if(cfg.gmp.to_build && cfg.gcc.to_build)
		fprintf(fp, "	download \"$GMP_URI\" \"$GMP_TARBALL\"\n");
	if(cfg.mpfr.to_build && cfg.gcc.to_build)
		fprintf(fp, "	download \"$MPFR_URI\" \"$MPFR_TARBALL\"\n");
	if(cfg.mpc.to_build && cfg.gcc.to_build)
		fprintf(fp, "	download \"$MPC_URI\" \"$MPC_TARBALL\"\n");
	if(cfg.gcc.to_build)
		fprintf(fp, "	download \"$GCC_URI\" \"$GCC_TARBALL\"\n");
	if(cfg.newlib.to_build)
		fprintf(fp, "	download \"$NEWLIB_URI\" \"$NEWLIB_TARBALL\"\n");
	if(cfg.gdb.to_build)
		fprintf(fp, "	download \"$GDB_URI\" \"$GDB_TARBALL\"\n");
	fprintf(fp, 
		"	\n"
		"	cleansrc\n"
		"	\n");
	if(cfg.binutils.to_build)
		fprintf(fp, "	extract \"$BINUTILS_TARBALL\" \"$TOOLDEV\"\n");
	if(cfg.newlib.to_build)
		fprintf(fp, "	extract \"$NEWLIB_TARBALL\" \"$TOOLDEV\"\n");
	if(cfg.gdb.to_build)
		fprintf(fp, "	extract \"$GDB_TARBALL\" \"$TOOLDEV\"\n");
	if(cfg.gcc.to_build) {
		fprintf(fp, "	extract \"$GCC_TARBALL\" \"$TOOLDEV\"\n");
		if(cfg.gmp.to_build)
			fprintf(fp, "	extract \"$GMP_TARBALL\" \"$TOOLDEV/$GCC_DIR\"\n"
				"	mv \"$TOOLDEV/$GCC_DIR/$GMP_DIR\" \"$TOOLDEV/$GCC_DIR/gmp\" || die \"Error renaming $GMP_DIR -> gmp\"\n");
		if(cfg.mpfr.to_build)
			fprintf(fp, "	extract \"$MPFR_TARBALL\" \"$TOOLDEV/$GCC_DIR\"\n"
				"	mv \"$TOOLDEV/$GCC_DIR/$MPFR_DIR\" \"$TOOLDEV/$GCC_DIR/mpfr\" || die \"Error renaming $MPFR_DIR -> mpfr\"\n");
		if(cfg.mpc.to_build)
			fprintf(fp, "	extract \"$MPC_TARBALL\" \"$TOOLDEV/$GCC_DIR\"\n"
				"	mv \"$TOOLDEV/$GCC_DIR/$MPC_DIR\" \"$TOOLDEV/$GCC_DIR/mpc\" || die \"Error renaming $MPC_DIR -> mpc\"\n");
	}
	fprintf(fp, 
		"	\n"
		"	cleanbuild\n"
		"	makedirs\n");
	if(cfg.binutils.to_build)
		fprintf(fp, 
		"	echo \"******* Building %s binutils\"\n"
		"	buildbinutils $TOOLCHAIN_TARGET\n", cfg.name);
	if(cfg.gcc.to_build && cfg.newlib.to_build)
		fprintf(fp, 
		"	echo \"******* Building %s Base GCC\"\n"
		"	buildgccppc $TOOLCHAIN_TARGET\n", cfg.name);
	if(cfg.newlib.to_build)
		fprintf(fp, 
		"	echo \"******* Building %s Newlib\"\n"
		"	buildnewlib $TOOLCHAIN_TARGET\n", cfg.name);
	if(cfg.gcc.to_build) {
		fprintf(fp, "	echo \"******* Building %s GCC\"\n", cfg.name);
		if(cfg.newlib.to_build)
			fprintf(fp, "	buildgccppc $TOOLCHAIN_TARGET --with-newlib\n");
		else
			fprintf(fp, "	buildgccppc $TOOLCHAIN_TARGET\n");
	}
	if(cfg.gdb.to_build)
		fprintf(fp, 
		"	echo \"******* Building %s GDB\"\n"
		"	buildgdb $TOOLCHAIN_TARGET\n", cfg.name);
	fprintf(fp, 
		"	echo \"******* %s toolchain built and installed\"\n"
	"}\n\n", cfg.name);
}

void usage(char *app)
{
	fprintf(stderr, "Usage:\n"
		"	%s config.txt outchain.sh [-v]\n",
		app);
}

int main(int argc, char* argv[])
{
	Toolchain toolchain;
	FILE* tcfp;
	
	if(argc < 3) {
		usage(argv[0]);
		return EXIT_FAILURE;
	}
	
	tcfp = fopen(argv[2], "wb");
	if(tcfp == NULL) {
		fprintf(stderr, "Can't open for writing\n");
		exit(EXIT_FAILURE);
	}
	
	load_toolchain_defaults(&toolchain);
	load_toolchain_script(&toolchain, argv[1]);
	
	if(argc > 3)
		if(strcmp(argv[3], "-v") == 0)
			print_toolchain(&toolchain);
	
	fprintf(tcfp, TOOLSCRIPT_HEADER);
	write_configuration(tcfp, toolchain);
	fprintf(tcfp, TOOLSCRIPT_TOP_SETTING);
	write_clean_subs(tcfp, toolchain);
	fprintf(tcfp, TOOLSCRIPT_DOWNLOAD_SUBS);
	write_makedirs_sub(tcfp, toolchain);
	write_buildbinutils_sub(tcfp, toolchain);
	write_buildgcc_sub(tcfp, toolchain);
	write_buildnewlib_sub(tcfp, toolchain);
	write_buildgdb_sub(tcfp, toolchain);
	write_buildtarget_sub(tcfp, toolchain);
	fprintf(tcfp, TOOLSCRIPT_FOOTER);
	fclose(tcfp);
	free_toolchain(&toolchain);
	
	return EXIT_SUCCESS;
}
